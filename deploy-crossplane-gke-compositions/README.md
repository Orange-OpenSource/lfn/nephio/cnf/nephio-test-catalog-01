# deploy-new-cluster

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] deploy-new-cluster`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree deploy-new-cluster`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init deploy-new-cluster
kpt live apply deploy-new-cluster --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
