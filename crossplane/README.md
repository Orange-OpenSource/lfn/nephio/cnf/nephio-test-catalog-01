# crossplane

## Description
Install crossplane.
<https://docs.crossplane.io/master/software/install/>

## Usage

### Fetch the package

```sh
kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] crossplane
```

Details: <https://kpt.dev/reference/cli/pkg/get/>

### View package content

`kpt pkg tree crossplane`
Details: <https://kpt.dev/reference/cli/pkg/tree/>

### Apply the package

```sh
kpt live init crossplane
kpt live apply crossplane --reconcile-timeout=2m --output=table
```

Details: <https://kpt.dev/reference/cli/live/>
