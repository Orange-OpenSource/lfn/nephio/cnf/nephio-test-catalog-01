# gitlab-deployment-repo

## Description
sample description

## Usage

### Fetch the package
`kpt pkg get REPO_URI[.git]/PKG_PATH[@VERSION] gitlab-deployment-repo`
Details: https://kpt.dev/reference/cli/pkg/get/

### View package content
`kpt pkg tree gitlab-deployment-repo`
Details: https://kpt.dev/reference/cli/pkg/tree/

### Apply the package
```
kpt live init gitlab-deployment-repo
kpt live apply gitlab-deployment-repo --reconcile-timeout=2m --output=table
```
Details: https://kpt.dev/reference/cli/live/
